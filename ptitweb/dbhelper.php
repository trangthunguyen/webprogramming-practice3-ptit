<?php
require_once('config.php');

// insert, update, delete
function execute($sql) {
    // create connection toi db
    $conn = mysqli_connect(HOST, USERNAME, PASSWORD, DATABASE);
    
    //query
    $result = mysqli_query($conn, $sql);

    //dong connection
    mysqli_close($conn);
    return $result;
}

//su dung cho lenh select
function executeResult($sql) {
    // create connection toi db
    $conn = mysqli_connect(HOST, USERNAME, PASSWORD, DATABASE);
    
    //query
    $resultset = mysqli_query($conn, $sql) or die( mysqli_error($conn));
    $list = [];
    while($row = mysqli_fetch_array($resultset, 1)){
        $list[] = $row;
    }
    //dong connection
    mysqli_close($conn);
    return $list;
}
?>